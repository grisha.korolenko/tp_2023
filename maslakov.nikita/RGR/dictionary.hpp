#pragma once
#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <string>
#include <map>

class Dictionary {
public:
    Dictionary();
    void insert(const std::string& word, const std::string& translation);
    std::string search(const std::string& word);
    bool remove(const std::string& word);
    void print();

private:
    std::map<std::string, std::string> words;
};

#endif
