#ifndef FREQUENCY_DICTIONARY
#define FREQUENCY_DICTIONARY

#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>

class FrequencyDictionary
{
private:
	std::unordered_map<std::string, size_t> dictionary_;
public:
	FrequencyDictionary();
	~FrequencyDictionary();
	size_t searchWord(const std::string& key);
	bool removeWord(const std::string& key);
	bool insertWord(const std::string& key);
	void threeMostFrequentWords(std::pair<std::string, size_t>* arr);
	void printMostFrequentWords();
	void printDictionary();
};

FrequencyDictionary::FrequencyDictionary() :dictionary_() {}

FrequencyDictionary::~FrequencyDictionary() {}

size_t FrequencyDictionary::searchWord(const std::string& key)
{
	auto it = dictionary_.find(key);
	if (it != dictionary_.end() && it->second > 0)
	{
		return it->second;
	}
	else
		return 0;
}

bool FrequencyDictionary::removeWord(const std::string& key)
{
	if (dictionary_.erase(key))
	{
		return true;
	}
	return false;
}

bool FrequencyDictionary::insertWord(const std::string& key)
{
	if ((dictionary_.find(key)) != dictionary_.end())
	{
		dictionary_.find(key)->second++;
	}
	else
	{
		dictionary_.insert({ key, 1 });
	}
	return true;
}

void FrequencyDictionary::threeMostFrequentWords(std::pair<std::string, size_t>* arr)
{
	auto it = dictionary_.begin();
	auto endIt = dictionary_.end();
	size_t minMaxThree = std::min({ arr[0].second, arr[1].second, arr[2].second });

	while (it != endIt)
	{
		if (it->second >= minMaxThree)
		{
			size_t minIndex = 0;
			size_t i = 0;
			while (true)
			{
				if (arr[i].second == minMaxThree)
				{
					minIndex = i;
					break;
				}
				i++;
			}

			arr[minIndex] = *it;
			minMaxThree = std::min({ arr[0].second, arr[1].second, arr[2].second });
		}

		++it;
	}
}

void FrequencyDictionary::printMostFrequentWords()
{
	std::pair<std::string, size_t> threeWords[3];
	threeMostFrequentWords(threeWords);
	if (threeWords[0].second == 0 || threeWords[1].second == 0 || threeWords[2].second == 0)
	{
		std::cout << "The dictionary lacks sufficient words to form a list of the three most frequently occurring words.\n";
	}
	else
	{
		std::cout << "Three most frequently occurring words:\n";
		std::cout << threeWords[0].first << " - " << threeWords[0].second << "\n";
		std::cout << threeWords[1].first << " - " << threeWords[1].second << "\n";
		std::cout << threeWords[2].first << " - " << threeWords[2].second << "\n";
	}
}

void FrequencyDictionary::printDictionary()
{
	for (auto i = dictionary_.begin(); dictionary_.begin() != dictionary_.end(); i++)
	{
		std::cout << "Key:[" << i->first << "] Value:[" << i->second << "]\n";
	}
}

#endif