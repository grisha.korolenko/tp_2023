﻿
#include <iostream>
#include <sstream>
#include <string>
#include <cassert>
#include <iterator>
#include <vector>
#include <iomanip>
#include <algorithm>

namespace nspace
{
    struct Data
    {
        double key1 = 0.0;
        long long key2 = 0;
        std::string key3;
    };

    struct DelimiterIO
    {
        char exp;
    };

    struct DoubleIO
    {
        double& ref;
    };

    struct LongLongIO
    {
        long long& ref;
    };

    struct StringIO
    {
        std::string& ref;
    };

    struct LabelIO
    {
        std::string &exp;
    };

    struct CompareDataStruct
    {
        bool operator()(const Data& first, const Data& second) const;
    };

    // scope guard для возврата состояния потока в первоначальное состояние
    class iofmtguard
    {
    public:
        iofmtguard(std::basic_ios< char >& s);
        ~iofmtguard();
    private:
        std::basic_ios< char >& s_;
        char fill_;
        std::streamsize precision_;
        std::basic_ios< char >::fmtflags fmt_;
    };

    std::istream& operator>>(std::istream& in, DelimiterIO&& dest);
    std::istream& operator>>(std::istream& in, DoubleIO&& dest);
    std::istream& operator>>(std::istream& in, LongLongIO&& dest);
    std::istream& operator>>(std::istream& in, StringIO&& dest);
    std::istream& operator>>(std::istream& in, LabelIO&& dest);
    std::istream& operator>>(std::istream& in, Data& dest);
    std::ostream& operator<<(std::ostream& out, const Data& dest);
}

int main()
{
    using nspace::Data;
    using nspace::CompareDataStruct;

    std::vector< Data > data;
    std::istringstream iss("(:key1 1.0d:key2 89ll:key3 \"Rightstr1\":)\n\
                            (:key1 3.9:key2 89LL:key3 \"str2\":)\n\
                            (:key2 89LL:key1 3.9d:key3 \"Rightstr3\":) \n\
                            (:key1 4.8d:key2 89lL:key3 \"str4\":)\n\
                            (:key1 3.9d:key2 89LLkey3 \"str5\":)\n\
                            (:key1 3.9d:key2 89LL:key3 \"str6:)\n\
                            (:key1 bkgd:key2 89LL:key3 \"str7\":)\n\
                            (:key1 3.9d:key2 89L L:key3 \"str8\":)\n\
                            (:key1 3.9d:key3 \"str9\":)\n\
                            (:key1 3.9d:key2 89 LL:key3 \"str10\":)\n\
                            (:key1 3.9d:key2 89 L L:key3 \"str11\":) (:key1 3.9d:key2 89LL:key3 \"Rightstr13\":)\n\
                            (:key1 3.9d:key2 89LL:key3 \"Rightstr14\":)\n\
                        ");

    while (!iss.eof())
    {
        if (!iss)
        {
            iss.clear();
        }
        std::copy(
            std::istream_iterator< Data >(iss),
            std::istream_iterator< Data >(),
            std::back_inserter(data)
        );
    }

    std::sort(
        std::begin(data),
        std::end(data),
        CompareDataStruct()
    );

    std::cout << "Data:\n";
    std::copy(
        std::begin(data),
        std::end(data),
        std::ostream_iterator< Data >(std::cout, "\n")
    );

    return 0;
}

namespace nspace
{
    std::istream& operator>>(std::istream& in, DelimiterIO&& dest)
    {
        // все перегрузки операторов ввода/вывода должны начинаться с проверки экземпляра класса sentry
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char c = '0';
        in >> c;
        if (in && (c != dest.exp))
        {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, DoubleIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char delim = '0';
        double num;
        in >> num >> std::noskipws >> delim >> std::skipws;
        if (!(delim == 'd' || delim == 'D'))
        {
            in.setstate(std::ios::failbit);
        }
        else
        {
            dest.ref = num;
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, LongLongIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char delim1 = '0';
        char delim2 = '0';
        long long num;
        in >> num >> std::noskipws >> delim1 >> delim2 >> std::skipws;
        if (!((delim1 == 'l' && delim2 == 'l') || (delim1 == 'L' && delim2 == 'L')))
        {
            in.setstate(std::ios::failbit);
        }
        else
        {
            dest.ref = num;
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, StringIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char c = '0';
        in >> c;
        if (in && c != '"')
        {
            in.setstate(std::ios::failbit);
        }
        
        std::string data;
        in >> std::noskipws >> c;
        while (in)
        {
            if (c == '\n' || c == '"')
            {
                break;
            }
            data += c;
            in >> c;
        }
        in >> std::skipws;

        if (in && c != '"')
        {
            dest.ref = "";
            in.setstate(std::ios::failbit);
        }
        else
        {
            dest.ref = data;
        }

        return in;
    }

    std::istream& operator>>(std::istream& in, LabelIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        std::string data;
        in >> data;
        std::string keyNum;
        bool isCorrect = false;
        for (int i = 1; i <= 3 && (!isCorrect); ++i)
        {
            keyNum = std::to_string(i);
            if (data == "key" + keyNum)
            {
                dest.exp = keyNum;
                isCorrect = true;
            }
        }
        if (!isCorrect)
        {
            dest.exp = "";
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, Data& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        iofmtguard formatGuard(in);
        {
            using sep = DelimiterIO;
            using label = LabelIO;
            using dbl = DoubleIO;
            using str = StringIO;
            using lnglng = LongLongIO;

            bool isFirst = false;
            bool isSecond = false;
            bool isThird = false;

            Data input;
            in >> std::skipws >> sep{ '(' } >> std::noskipws >> sep{ ':' } >> std::skipws;
            for (int i = 0; i < 3; ++i)
            {
                std::string numKey;
                in >> label{ numKey };
                if (numKey == "1")
                {
                    if (isFirst)
                    {
                        in.setstate(std::ios::failbit);
                        break;
                    }
                    in >> dbl{ input.key1 };
                    isFirst = true;
                }
                else if (numKey == "2")
                {
                    if (isSecond)
                    {
                        in.setstate(std::ios::failbit);
                        break;
                    }
                    in >> lnglng{ input.key2 };
                    isSecond = true;
                }
                else if (numKey == "3")
                {
                    if (isThird)
                    {
                        in.setstate(std::ios::failbit);
                        break;
                    }
                    in >> str{ input.key3 };
                    isThird = true;
                }
                in >> sep{ ':' };
            }

            in >> sep{ ')' };
            if (in)
            {
                dest = input;
            }
            return in;
        }
    }

    std::ostream& operator<<(std::ostream & out, const Data & src)
    {
            std::ostream::sentry sentry(out);
            if (!sentry)
            {
                return out;
            }
            iofmtguard fmtguard(out);
            out << "(: ";
            out << "key1 " << std::fixed << std::setprecision(1) << src.key1 << "d:";
            out << "key2 " << src.key2 << "ll:";
            out << "key3 " << '\"' << src.key3 << '\"';
            out << " :)";
            return out;
    }
    bool CompareDataStruct::operator()(const Data& first, const Data& second) const
    {
        if (first.key1 == second.key1 && first.key2 == second.key2)
        {
            return first.key3.length() < second.key3.length();
        }
        if (first.key1 == second.key1)
        {
            return first.key2 < second.key2;
        }
        return first.key1 < second.key1;
    }

    iofmtguard::iofmtguard(std::basic_ios< char >& s) :
        s_(s),
        fill_(s.fill()),
        precision_(s.precision()),
        fmt_(s.flags())
    {}

    iofmtguard::~iofmtguard()
    {
        s_.fill(fill_);
        s_.precision(precision_);
        s_.flags(fmt_);
    }
}
