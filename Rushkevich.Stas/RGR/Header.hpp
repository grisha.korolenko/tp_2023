#ifndef HEADER
#define HEADER

#include <iostream>
#include <string>
#include <list>
#include <memory>
#include <algorithm>

struct Node
{
    std::string key;
    int balance;
    std::list<std::string> value;
    std::unique_ptr<Node> left;
    std::unique_ptr<Node> right;
};

class AVLTree
{
public:
    std::unique_ptr<Node> root;

    AVLTree() : root(nullptr) {}
    ~AVLTree() = default;

    void insert(const std::string &key, const std::string &value)
    {
        std::list<std::string> values = {value};
        root = insert(std::move(root), key, values);
    }

    void remove(const std::string &key)
    {
        root = remove(std::move(root), key);
    }

    std::list<std::string> search(const std::string &key) const
    {
        Node *node = search(root.get(), key);
        if (node == nullptr)
        {
            return std::list<std::string>();
        }
        return node->value;
    }

private:
    std::unique_ptr<Node> insert(std::unique_ptr<Node> node, const std::string &key, const std::list<std::string> &values)
    {
        if (node == nullptr)
        {
            return std::make_unique<Node>(Node{key, 0, values, nullptr, nullptr});
        }
        if (key < node->key)
        {
            node->left = insert(std::move(node->left), key, values);
        }
        else if (key > node->key)
        {
            node->right = insert(std::move(node->right), key, values);
        }
        else
        {
            for (auto const &value : values)
            {
                node->value.push_back(value);
            }

            return node;
        }

        node->balance = height(node->left) - height(node->right);

        if (node->balance == 2)
        {
            if (height(node->left->left) >= height(node->left->right))
            {
                node = leftRotate(std::move(node));
            }
            else
            {
                node->left = rightRotate(std::move(node->left));
                node = leftRotate(std::move(node));
            }
        }
        else if (node->balance == -2)
        {
            if (height(node->right->right) >= height(node->right->left))
            {
                node = rightRotate(std::move(node));
            }
            else
            {
                node->right = leftRotate(std::move(node->right));
                node = rightRotate(std::move(node));
            }
        }

        return node;
    }

    std::unique_ptr<Node> remove(std::unique_ptr<Node> node, const std::string &key)
    {
        if (node == nullptr)
        {
            return nullptr;
        }

        if (key < node->key)
        {
            node->left = remove(std::move(node->left), key);
        }
        else if (key > node->key)
        {
            node->right = remove(std::move(node->right), key);
        }
        else
        {
            if (node->left == nullptr && node->right == nullptr)
            {
                return nullptr;
            }
            else if (node->left != nullptr && node->right == nullptr)
            {
                return std::move(node->left);
            }
            else if (node->left == nullptr && node->right != nullptr)
            {
                return std::move(node->right);
            }
            else
            {
                Node *temp = findMin(node->right.get());
                node->key = temp->key;
                node->value = temp->value;
                node->right = remove(std::move(node->right), temp->key);
            }
        }

        node->balance = height(node->left) - height(node->right);
        if (node->balance == 2)
        {
            if (height(node->left->left) >= height(node->left->right))
            {
                node = leftRotate(std::move(node));
            }
            else
            {
                node->left = rightRotate(std::move(node->left));
                node = leftRotate(std::move(node));
            }
        }
        else if (node->balance == -2)
        {
            if (height(node->right->right) >= height(node->right->left))
            {
                node = rightRotate(std::move(node));
            }
            else
            {
                node->right = leftRotate(std::move(node->right));
                node = rightRotate(std::move(node));
            }
        }

        return node;
    }

    Node *search(Node *node, const std::string &key)const
    {
        if (node == nullptr || node->key == key)
        {
            return node;
        }

        if (key < node->key)
        {
            return search(node->left.get(), key);
        }
        else
        {
            return search(node->right.get(), key);
        }
    }

    int height(const std::unique_ptr<Node> &node) const
    {
        if (node == nullptr)
        {
            return 0;
        }
        return 1 + std::max(height(node->left), height(node->right));
    }

    std::unique_ptr<Node> leftRotate(std::unique_ptr<Node> node)
    {
        std::unique_ptr<Node> temp = std::move(node->left);
        node->left = std::move(temp->right);
        temp->right = std::move(node);
        return temp;
    }

    std::unique_ptr<Node> rightRotate(std::unique_ptr<Node> node)
    {
        std::unique_ptr<Node> temp = std::move(node->right);
        node->right = std::move(temp->left);
        temp->left = std::move(node);
        return temp;
    }

    Node *findMin(Node *node)
    {
        while (node->left != nullptr)
        {
            node = node->left.get();
        }
        return node;
    }
};

#endif