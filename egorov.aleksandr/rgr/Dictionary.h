#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <string>
#include "AVLTree.h"

class Dictionary {
private:
	AVLTree tree_;

public:
	Dictionary() : tree_() {}

	void insert(std::string key, std::string value) {
		tree_.insert(key, value);
	}

	std::list<std::string>* search(std::string key) {
		return tree_.search(key);
	}

	void deleteKey(std::string key) {
		tree_.deleteKey(key);
	}

	int getHeight() {
		return tree_.get_height(tree_.dict);
	}

	void saveToFile(std::string filename) {
		tree_.saveToFile(filename);
	}
};

#endif // DICTIONARY_H
