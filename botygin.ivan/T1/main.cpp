﻿#include <iostream>
#include <sstream>
#include <string>
#include <cassert>
#include <iterator>
#include <vector>
#include <iomanip>
#include <algorithm>
#include "DataStruct.h"

int main()
{

    std::vector< Data > data;
    std::istringstream iss(
        "{:key1 0:key2 0X0:key3 \"\":}\n"
        "{:key1 99999999999999999999:key2 0XFFFFFFFFFFFFFFFFF:key3 \"Error\":}\n"
        "{:key1 1.00e-01:key2 0X0:key3 \"1\":}\n"
        "{:key1 1.02e-01:key2 0X0:key3 \"1\":}\n"
        "{:key1 1.01e-01:key2 0X0:key3 \"1\":}\n"
        "{:key1 1.02e-01:key2 0X1:key3 \"1\":}\n"
        "{:key1 1.02e-01:key2 0X2:key3 \"1\":}\n"
        "{:key1 1.02e-01:key2 0X2:key3 \"22\":}\n"
        "{:key3 \"ReversedKeys\":key2 0X2:key1 5.01e-23:}\n"
        "{:key1 1.02ee-01:key2 0X2:key3 \"Error\":}\n"
        "{:key1 1.02e-01:key2 0XError:key3 \"Error\":}\n"
        "{:key1 5.02e+02:key2 0X123:key3 \"CorrectStruct\":}\n"
    );
    while (!iss.eof()) {
        std::copy(
            std::istream_iterator< Data >(iss),
            std::istream_iterator< Data >(),
            std::back_inserter(data)
        );
        bool eofFlag = iss.eof();
        iss.clear();
        if (eofFlag) {
            iss.setstate(std::ios::eofbit);
        }
        iss.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    std::cout << "Data:\n";
    std::sort(data.begin(), data.end());
    std::copy(
        std::begin(data),
        std::end(data),
        std::ostream_iterator< Data >(std::cout, "\n")
    );

    return 0;
}