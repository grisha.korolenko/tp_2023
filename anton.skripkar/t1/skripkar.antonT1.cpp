﻿// skripkar.antonT1.cpp : Вриант 17 [ULL HEX] [CHR LIT] 
#include<iostream>
#include<string>
#include<vector>
#include <algorithm>
#include <bitset>
#include <iomanip>
#include <sstream>
#include <iterator>

struct DataStruct
{
	unsigned long long key1;
	char key2;
	std::string key3;
};

struct CompareDataStruct
{// Определение оператора сравнения для структуры DataStruct 
	bool operator()(const DataStruct& ds1, const DataStruct& ds2) const
	{
		if (ds1.key1 != ds2.key1)
		{
			return ds1.key1 < ds2.key1;
		}
		else if (ds1.key2 != ds2.key2)
		{
			return ds1.key2 < ds2.key2;
		}
		else
		{
			return ds1.key3.size() < ds2.key3.size();
		}
	}
};

// Определение перегрузки оператора для ввода, которые  позволяют считывать и выводить объекты типа DataStruct из/в поток.
std::istream& operator>> (std::istream& is, DataStruct& ds)
{
	is >> ds.key1 >> ds.key2 >> ds.key3;
	return is;
}

// Определение оператора << для вывода Datastruct в поток 
std::ostream& operator<<(std::ostream& os, const DataStruct& ds) 
{
	os << ds.key1 << "" << ds.key2 << "" << ds.key3;
	return os;
}



int main()
{
	// входные данные для инициализации std::vector<DataStruct> data
	/*std::string input_string = "(:key1 10ull:key2 'c':key3 \"Data\":)\n(:key3 \"Data\":key2 'c':key1 10ull:)";*/
	std::string input_string =  "10 c Data\n20 d Input\n 10 c Diskription\n";
	std::istringstream iss(input_string);

	std::vector<DataStruct> data;

	// считывание данных из стандартного вывода и сохранение их в векторе
	std::copy(std::istream_iterator<DataStruct>(iss), std::istream_iterator<DataStruct>(), std::back_inserter(data));

	// сортирует вектор по заданным критериям
	std::sort(data.begin(), data.end(), CompareDataStruct());

	// для вывода отсортированных данных в стандартный ввод
	std::copy(data.begin(), data.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));

	return 0;

}