﻿#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <functional>
#include <numeric>
#include <iterator>

struct Point
{
	int x_;
	int y_;
};
struct Polygon
{
	std::vector< Point > points;
};

struct DelimiterIO
{
	char exp;
};

struct PointIO
{
	Point& ref;
};

class iofmtguard
{
public:
	iofmtguard(std::basic_ios< char >& s);
	~iofmtguard();
private:
	std::basic_ios< char >& s_;
	char fill_;
	std::streamsize precision_;
	std::basic_ios< char >::fmtflags fmt_;
};

std::istream& operator>>(std::istream& in, DelimiterIO&& dest);
std::istream& operator>>(std::istream& in, PointIO&& dest);
std::istream& operator>>(std::istream& in, Polygon& dest);
std::ostream& operator<<(std::ostream& out, const Polygon& dest);
bool operator==(const Polygon& left, const Polygon& right);
bool isEqualPoint(const Point& p1, const Point& p2);
size_t getNumOfVertexes(const Polygon& polygon);
double getArea(const Polygon& polygon);
double areaEven(const std::vector< Polygon >& polygons);
double areaOdd(const std::vector< Polygon >& polygons);
double areaMean(const std::vector< Polygon >& polygons);
double areaVertexes(const std::vector< Polygon >& polygons, size_t numOfVertexes);
double maxArea(const std::vector< Polygon >& polygons);
size_t maxVertexes(const std::vector< Polygon >& polygons);
double minArea(const std::vector< Polygon >& polygons);
size_t minVertexes(const std::vector< Polygon >& polygons);
size_t countEven(const std::vector< Polygon >& polygons);
size_t countOdd(const std::vector< Polygon >& polygons);
size_t countNumOfVertexes(const std::vector< Polygon >& polygons, size_t numOfVertexes);
size_t echo(std::vector< Polygon >& polygons, Polygon argument);
bool inFrame(const std::vector< Polygon >& polygons, Polygon argument);
void doCommands(std::vector< Polygon >& polygons, std::istream& in);

int main()
{
	std::istringstream iss("    3 (1;1) (1;3)  \n\
								4 (0; 0) (0; 1) (1; 1) (1; 0) \n\
								5 (5; 5) (6; 8) (8; 8) (8; 3) (6; 3) \n\
								4 (0; 0) (0; 1) (1; 1) (1; 0) \n\
								4 not correct line 1) (1; 1) (1; 0) \n\
								3 (0; 0) (-2; 0) (0; -2)  ");

	std::string line;
	std::stringstream streamLine;
	std::vector< Polygon > data;

	while (!iss.eof())
	{
		std::getline(iss, line);
		streamLine.clear();
		streamLine << line;
		Polygon polygon;
		streamLine >> polygon;
		if (streamLine)
		{
			data.push_back(polygon);
		}
		else
		{
			streamLine.clear();
			streamLine.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
	}

	std::copy(
		std::begin(data),
		std::end(data),
		std::ostream_iterator< Polygon >(std::cout, "\n")
	);

	std::cout << '\n';
	std::istringstream commands(" 	AREA ODD\n\
									456\n\
									AREA EVEN\n\
									AREA MEAN\n\
									AREA 4\n\
									AREA 7\n\
									AREA 3\n\
									COUNT ODD\n\
									COUNT EVEN\n\
									COUNT 3\n\
									COUNT 4\n\
									COUNT yy\n\
									MAX AREA\n\
									MAX VERTEXES\n\
									MIN AREA\n\
									MIN VERTEXES\n\
									INFRAME 3 (0;0) (2;2) (2;0)\n\
									INFRAME 3 (-10;-1) (1;1) (1;0)\n\
									ECHO 4 (0; 0) (0; 1) (1; 1) (1; 0)\n\
									COUNT EVEN");
	iofmtguard guard(std::cout);
	std::cout << std::fixed << std::setprecision(1);
	while (!commands.eof())
	{
		try {
			doCommands(data, commands);
		}
		catch (const std::invalid_argument& e) {
			std::cerr << e.what() << '\n';
			commands.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
	}
	std::cout << "\nnow we have:\n";
	std::copy(
		std::begin(data),
		std::end(data),
		std::ostream_iterator< Polygon >(std::cout, "\n")
	);
	return 0;
}


std::istream& operator>>(std::istream& in, DelimiterIO&& dest)
{
	std::istream::sentry sentry(in);
	if (!sentry)
	{
		return in;
	}
	char c = '0';
	in >> c;
	if (in && (c != dest.exp))
	{
		in.setstate(std::ios::failbit);
	}
	return in;
}

std::istream& operator>>(std::istream& in, PointIO&& dest)
{
	std::istream::sentry sentry(in);
	if (!sentry)
	{
		return in;
	}
	return in >> DelimiterIO{ '(' } >> dest.ref.x_ >> DelimiterIO{ ';' } >> dest.ref.y_ >> DelimiterIO{ ')' };
}

std::istream& operator>>(std::istream& in, Polygon& dest)
{
	std::istream::sentry sentry(in);
	if (!sentry)
	{
		return in;
	}
	Polygon input;
	size_t numberOfPoints = 0;
	size_t count = 0;
	in >> numberOfPoints;
	if (numberOfPoints < 1)
	{
		in.setstate(std::ios::failbit);
	}
	for (size_t i = 0; i < numberOfPoints && in; i++)
	{
		Point point;
		in >> PointIO{ point };
		if (in)
		{
			count++;
			input.points.push_back(point);
		}
	}
	if (in && count == numberOfPoints)
	{
		dest = input;
	}
	return in;
}

std::ostream& operator<<(std::ostream& out, const Polygon& src)
{
	std::ostream::sentry sentry(out);
	if (!sentry)
	{
		return out;
	}
	iofmtguard fmtguard(out);
	out << src.points.size() << " ";
	for (size_t i = 0; i < src.points.size(); i++)
	{
		out << "(" << src.points[i].x_ << ";" << src.points[i].y_ << ") ";
	}
	return out;
}

bool isEqualPoint(const Point& p1, const Point& p2)
{
	return p1.x_ == p2.x_ && p1.y_ == p2.y_;
}

bool operator==(const Polygon& left, const Polygon& right)
{
	if (left.points.size() == right.points.size())
	{
		auto iterator = std::mismatch(left.points.begin(), left.points.end(),
			right.points.begin(), right.points.end(), isEqualPoint);
		return (iterator.first == left.points.end());
	}
	else
	{
		return false;
	}
}

iofmtguard::iofmtguard(std::basic_ios< char >& s) :
	s_(s),
	fill_(s.fill()),
	precision_(s.precision()),
	fmt_(s.flags())
{}

iofmtguard::~iofmtguard()
{
	s_.fill(fill_);
	s_.precision(precision_);
	s_.flags(fmt_);
}

size_t getNumOfVertexes(const Polygon& polygon)
{
	return polygon.points.size();
}

double getArea(const Polygon& polygon)
{
	size_t n = polygon.points.size();
	double area = 0.0;
	for (size_t i = 0; i < n; i++)
	{
		size_t j = (i + 1) % n;
		area += polygon.points[i].x_ * polygon.points[j].y_;
		area -= polygon.points[j].x_ * polygon.points[i].y_;
	}
	area = std::abs(area) / 2.0;
	return area;
}

double areaEven(const std::vector< Polygon >& polygons)
{
	double result = std::accumulate(polygons.begin(), polygons.end(), 0.0,
		[](double res, const Polygon& polygon)
		{
			res += ((polygon.points.size() % 2 == 0) ? getArea(polygon) : 0.0);
			return res;
		}
	);
	return result;
}

double areaOdd(const std::vector< Polygon >& polygons)
{
	double result = std::accumulate(polygons.begin(), polygons.end(), 0.0,
		[](double res, const Polygon& polygon)
		{
			res += ((polygon.points.size() % 2 == 1) ? getArea(polygon) : 0.0);
			return res;
		}
	);
	return result;
}

double areaMean(const std::vector< Polygon >& polygons)
{
	double areaOfAll = std::accumulate(polygons.begin(), polygons.end(), 0.0,
		[](double res, const Polygon& polygon)
		{ return res + getArea(polygon); }
	);
	return areaOfAll / polygons.size();
}

double areaVertexes(const std::vector< Polygon >& polygons, size_t numOfVertexes)
{
	double result = std::accumulate(polygons.begin(), polygons.end(), 0.0,
		[numOfVertexes](double res, const Polygon& polygon)
		{
			res += (polygon.points.size() == numOfVertexes) ? getArea(polygon) : 0.0;
			return res;
		}
	);
	return result;
}

double maxArea(const std::vector< Polygon >& polygons)
{
	auto largest = std::max_element (
		polygons.begin(), 
		polygons.end(),
		std::bind (
				std::less<double>(),
				std::bind(getArea, std::placeholders::_1),
				std::bind(getArea, std::placeholders::_2)
			)
		);
	return getArea(*largest);
}

size_t maxVertexes(const std::vector< Polygon >& polygons)
{
	auto largest = std::max_element(
		polygons.begin(),
		polygons.end(),
		std::bind(
			std::less<double>(),
			std::bind(getNumOfVertexes, std::placeholders::_1),
			std::bind(getNumOfVertexes, std::placeholders::_2)
		)
	);
	return getNumOfVertexes(*largest);
}

double minArea(const std::vector< Polygon >& polygons)
{
	auto smallest = std::min_element(
		polygons.begin(),
		polygons.end(),
		std::bind(
			std::less<double>(),
			std::bind(getArea, std::placeholders::_1),
			std::bind(getArea, std::placeholders::_2)
		)
	);
	return getArea(*smallest);
}

size_t minVertexes(const std::vector< Polygon >& polygons)
{
	auto smallest = std::min_element(
		polygons.begin(),
		polygons.end(),
		std::bind(
			std::less<double>(),
			std::bind(getNumOfVertexes, std::placeholders::_1),
			std::bind(getNumOfVertexes, std::placeholders::_2)
		)
	);
	return getNumOfVertexes(*smallest);
}

size_t countEven(const std::vector< Polygon >& polygons)
{
	return std::count_if(polygons.begin(), polygons.end(), [](const Polygon& polygon)
		{ return polygon.points.size() % 2 == 0; });
}

size_t countOdd(const std::vector< Polygon >& polygons)
{
	return std::count_if(polygons.begin(), polygons.end(), [](const Polygon& polygon)
		{ return polygon.points.size() % 2 == 1; });
}

size_t countNumOfVertexes(const std::vector< Polygon >& polygons, size_t numOfVertexes)
{
	return std::count_if(polygons.begin(), polygons.end(), [numOfVertexes](const Polygon& polygon)
		{ return polygon.points.size() == numOfVertexes; });
}

size_t echo(std::vector< Polygon >& polygons, Polygon argument)
{
	if (polygons.size() == 0)
	{
		return 0;
	}
	std::vector<Polygon> copies;
	std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(copies), [&argument](const Polygon& polygon)
		{ return polygon == argument; });
	size_t result = copies.size();
	if (result > 0)
	{
		std::vector<Polygon> newPolygons;
		std::transform(polygons.begin(), polygons.end(), std::back_inserter(newPolygons),
			[&argument, &newPolygons](Polygon& object)
			{
				if (object == argument)
				{
					newPolygons.push_back(object);
				}
				return object;
			});
		std::swap(polygons, newPolygons);
	}
	return result;
}

bool inFrame(const std::vector< Polygon >& polygons, Polygon argument)
{
	struct frame_t
	{
		int xMax;
		int xMin;
		int yMax;
		int yMin;
	};
	Point firstPoint = polygons[0].points[0];
	frame_t mainFrame
	{
		firstPoint.x_,
		firstPoint.x_,
		firstPoint.y_,
		firstPoint.y_
	};
	mainFrame = std::accumulate(polygons.begin(), polygons.end(), mainFrame,
		[](frame_t frame, const Polygon& polygon)
		{
			return std::accumulate(polygon.points.begin(),	polygon.points.end(), frame,
				[](frame_t frame, const Point& point)
				{
					frame.xMax = std::max(frame.xMax, point.x_);
					frame.xMin = std::min(frame.xMin, point.x_);
					frame.yMax = std::max(frame.yMax, point.y_);
					frame.yMin = std::min(frame.yMin, point.y_);
					return frame;
				}
			);
		});

	return std::all_of(argument.points.begin(), argument.points.end(),
		[mainFrame](const Point& point)
		{return point.x_ >= mainFrame.xMin && point.x_ <= mainFrame.xMax && point.y_ >= mainFrame.yMin && point.y_ <= mainFrame.yMax;}
	);
}

void doCommands(std::vector< Polygon >& polygons, std::istream& in)
{
	std::string INVALID_COMMAND = "INVALID COMMAND";
	std::string command;
	in >> command;
	if (command == "AREA")
	{
		std::string argument;
		in >> argument;
		if (argument == "EVEN")
		{
			std::cout << "AREA EVEN: " << areaEven(polygons) << '\n';
		}
		else if (argument == "ODD")
		{
			std::cout << "AREA ODD: " << areaOdd(polygons) << '\n';
		}
		else if (argument == "MEAN")
		{
			if (polygons.size() == 0)
			{
				throw std::invalid_argument("NOT ENOUGH FIGURES TO GET MEAN AREA\n");
			}
			std::cout << "AREA MEAN: " << areaMean(polygons) << '\n';
		}
		else
		{
			size_t vertexes = 0;
			try
			{
				vertexes = std::stoull(argument);
			}
			catch (...)
			{
				throw std::invalid_argument(INVALID_COMMAND);
			}
			std::cout << "AREA " << vertexes << ": " << areaVertexes(polygons, vertexes) << '\n';
		}
	}
	else if (command == "MAX")
	{
		if (polygons.size() == 0)
		{
			throw std::invalid_argument("NOT ENOUGH FIGURES TO GET MAX\n");
		}
		std::string argument;
		in >> argument;
		if (argument == "AREA")
		{
			std::cout << "MAX AREA: " << maxArea(polygons) << '\n';
		}
		else if (argument == "VERTEXES")
		{
			std::cout << "MAX VERTEXES: " << maxVertexes(polygons) << '\n';
		}
		else
		{
			throw std::invalid_argument(INVALID_COMMAND);
		}
	}
	else if (command == "MIN")
	{
		if (polygons.size() == 0)
		{
			throw std::invalid_argument("NOT ENOUGH FIGURES TO GET MIN\n");
		}
		std::string argument;
		in >> argument;
		if (argument == "AREA")
		{
			std::cout << "MIN AREA: " << minArea(polygons) << '\n';
		}
		else if (argument == "VERTEXES")
		{
			std::cout << "MIN VERTEXES: " << minVertexes(polygons) << '\n';
		}
		else
		{
			throw std::invalid_argument(INVALID_COMMAND);
		}
	}
	else if (command == "COUNT")
	{
		std::string argument;
		in >> argument;
		if (argument == "EVEN")
		{
			std::cout << "COUNT EVEN: " << countEven(polygons) << '\n';
		}
		else if (argument == "ODD")
		{
			std::cout << "COUNT ODD: " << countOdd(polygons) << '\n';
		}
		else
		{
			size_t vertexes = 0;
			try
			{
				vertexes = std::stoull(argument);
			}
			catch (...)
			{
				throw std::invalid_argument(INVALID_COMMAND);
			}
			std::cout << "COUNT " << vertexes << ": " << countNumOfVertexes(polygons, vertexes) << '\n';
		}
	}
	else if (command == "ECHO")
	{
		Polygon argument;
		in >> argument;
		if (!in)
		{
			throw std::invalid_argument(INVALID_COMMAND);
		}
		std::cout << "ECHO  " << argument << ": " << echo(polygons, argument) << '\n';
	}
	else if (command == "INFRAME")
	{
		if (polygons.size() == 0)
		{
			throw std::invalid_argument("NOT ENOUGH FIGURES TO GET FRAME\n");
		}
		Polygon argument;
		in >> argument;
		if (!in)
		{
			throw std::invalid_argument(INVALID_COMMAND);
		}
		std::cout << "INFRAME  " << argument << ": " << (inFrame(polygons, argument) ? "<TRUE>" : "<FALSE>") << '\n';
	}
	else
	{
		in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}
}



