#ifndef ENG_RUS_DICTIONARY_H
#define ENG_RUS_DICTIONARY_H
#include <set>
#include <iostream>
#include <unordered_map>
#include <string>

class EngRusDictionary
{
private:
	std::unordered_map <std::string, std::set<std::string>> hashTable_;
public:
	bool insertWord(const std::string& word, const std::string& translation);
	void searchWord(const std::string& word) const;
	bool deleteWord(const std::string& word);
	void printDictionary() const;
};


#endif