﻿#include <iostream>
#include <fstream>
#include <complex>
#include <vector>
#include <iterator>
#include <iomanip>
#include <string>
#include <algorithm>

struct DataStruct
{
    unsigned long long   key1;
    unsigned long long   key2;
    std::string          key3;
};

struct DataStructComparator
{
    bool operator()(const DataStruct& left, const DataStruct& right);
};

struct DelimiterIO
{
    char exp;
};

struct DigitIO
{
    char& ref;
};

struct UnsignedLongLongIO
{
    unsigned long long& ref;
};

struct UnsignedLongLong8IO
{
    unsigned long long& ref;
};

struct DoubleIO
{
    int& ref;
};

struct StringIO
{
    std::string& ref;
};

struct LabelIO
{
    std::string exp;
};

class iofmtguard
{
public:
    iofmtguard(std::basic_ios< char >& s);
    ~iofmtguard();
private:
    std::basic_ios< char >& s_;
    char fill_;
    std::streamsize precision_;
    std::basic_ios< char >::fmtflags fmt_;
};

std::istream& operator>>(std::istream& in, DelimiterIO&& dest);
std::istream& operator>>(std::istream& in, DigitIO&& dest);
std::istream& operator>>(std::istream& in, UnsignedLongLongIO&& dest);
std::istream& operator>>(std::istream& in, UnsignedLongLong8IO&& dest);
std::istream& operator>>(std::istream& in, DoubleIO&& dest);
std::istream& operator>>(std::istream& in, StringIO&& dest);
std::istream& operator>>(std::istream& in, LabelIO&& dest);
std::istream& operator>>(std::istream& in, DataStruct& dest);
std::ostream& operator<<(std::ostream& out, const DataStruct& src);
bool isOctal(int num);

int main()
{
    std::vector< DataStruct > dataStructs;
    std::istringstream iss(
        "\n  (:key1 7ull:key2 011:key3 \"first\":)           \
        \n  (:key2 023:key3 \"second\":key1 25ull:)         \
        \n  (:key2 0?55:key3 \"third\":key1 344ull:)        \
        \n  (:key2 075:key1 344ull:key3 \"fourth\":)        \
        \n  (:key2 025:key1 344ull:key3 \"fifth\":)         \
        \n  (:key1 62343ull:key3 \"sixth\":key2 073:)       \
        \n  (:key1 9ull:key4 097:key3 \"err1\":)		    неверный формат ключа\
        \n  (:key2 056:key2 054:key3 \"err2\":)		        key2 повтор\
        \n  (:key1 \"err3\":key2 0ull:key3 0234:)		    неверные значения после ключей\
        \n  (:key1 - 1ull:key2 054:key3 \"err4\":)          неверный формат первого ключа\
        \n  (:key1 0ull:key2 014:key3 \"e\"rr\"505\":)	    неверный формат третьего ключа\
        \n  (:key1 0ull key2 020) key3 \"err512\":)			без разделителей и прочие ошибки в оформлении: \
        \n  (:key1 9ull:key2 894:key3 \"err404:)            \
        \n  (:key1 0ull:key2 00:key3 \"incorrect10:)		\
        \n  (:ke                                            \
        \n  (:key2 0 ? 55:key3 \"third\":key1 6ull:)"
    );
    if (iss.eof())
    {
        std::cerr << "String is empty\n";
        return -1;
    }
    do
    {
        if (!iss)
        {
            iss.clear();
            iss.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        std::copy(
            std::istream_iterator< DataStruct >(iss),
            std::istream_iterator< DataStruct >(),
            std::back_inserter(dataStructs)
        );
    } while (!iss.eof());

    std::sort(dataStructs.begin(), dataStructs.end(), DataStructComparator());
    std::cout << "Sorted data:\n";
    std::copy(
        std::begin(dataStructs),
        std::end(dataStructs),
        std::ostream_iterator< DataStruct >(std::cout, "\n")
    );

    return 0;
}

std::istream& operator>>(std::istream& in, DelimiterIO&& dest)
{
    std::istream::sentry sentry(in);
    if (!sentry)
    {
        return in;
    }
    char c = '0';
    in >> c;
    if (in && (c != dest.exp))
    {
        if (c == '\n')
        {
            in.unget();
        }
        in.setstate(std::ios::failbit);
    }
    return in;
}

std::istream& operator>>(std::istream& in, DigitIO&& dest)
{
    std::istream::sentry sentry(in);
    if (!sentry)
    {
        return in;
    }
    char c = '\0';
    in >> c;
    if (in && (c < '0' || c > '9'))
    {
        in.setstate(std::ios::failbit);
    }
    else
    {
        dest.ref = c;
    }
    return in;
}

std::istream& operator>>(std::istream& in, UnsignedLongLongIO&& dest)
{
    std::istream::sentry sentry(in);
    if (!sentry)
    {
        return in;
    }
    unsigned long long number = 0;
    if (in.peek() != '-')
    {
        in >> number >> LabelIO{ "ull" };
        if (in)
        {
            dest.ref = number;
        }
    }
    else
    {
        in.setstate(std::ios::failbit);
    }
    return in;
}

bool isOctal(int num) {
    std::ostringstream iss;
    iss << num;
    for (char c : iss.str()) {
        if (c < '0' || c > '7') {
            return false;
        }
    }
    return true;
}

std::istream& operator>>(std::istream& in, UnsignedLongLong8IO&& dest)
{
    std::istream::sentry sentry(in);
    if (!sentry)
    {
        return in;
    }
    unsigned long long number = 0;
    if (in.peek() != '-')
    {
        in >> LabelIO{ "0" } >> number;
        if (in /*&& isOctal(number)*/)
        {
            dest.ref = number;
        }
    }
    else
    {
        in.setstate(std::ios::failbit);
    }
    return in;
}

std::istream& operator>>(std::istream& in, DoubleIO&& dest)
{
    std::istream::sentry sentry(in);
    if (!sentry)
    {
        return in;
    }
    return in >> dest.ref;
}

std::istream& operator>>(std::istream& in, StringIO&& dest)
{
    std::istream::sentry sentry(in);
    if (!sentry)
    {
        return in;
    }
    std::string str = "";
    char c = '\0';
    in >> DelimiterIO{ '"' };
    while (in)
    {
        c = in.get();
        if (c == '"')
        {
            dest.ref = str;
            break;
        }
        else if (c == '\n')
        {
            in.unget();
            in.setstate(std::ios::failbit);
            break;
        }
        else
        {
            str.push_back(c);
        }
    }
    return in;
}

std::istream& operator>>(std::istream& in, LabelIO&& dest)
{
    std::istream::sentry sentry(in);
    if (!sentry)
    {
        return in;
    }
    const size_t BUF_SIZE = dest.exp.size();
    std::unique_ptr<char> buf(new char[BUF_SIZE + 1] {});
    in.read(buf.get(), BUF_SIZE);
    std::string data(buf.get());
    buf.reset();
    if (in && (data != dest.exp))
    {
        in.setstate(std::ios::failbit);
    }
    return in;
}

std::istream& operator>>(std::istream& in, DataStruct& dest)
{
    std::istream::sentry sentry(in);
    if (!sentry)
    {
        return in;
    }
    using separate = DelimiterIO;
    using digit = DigitIO;
    using label = LabelIO;
    using ull = UnsignedLongLongIO;
    using ull8 = UnsignedLongLong8IO;
    using dbl = DoubleIO;
    using str = StringIO;
    DataStruct input;
    in >> std::skipws >> separate{ '(' } >> std::noskipws >> separate{ ':' };
    bool keyOne = false;
    bool keyTwo = false;
    bool keyThree = false;
    for (int i = 0; i < 3; ++i)
    {
        in >> label{ "key" };
        char numOfKey = '\0';
        in >> digit{ numOfKey } >> separate{ ' ' };
        if (numOfKey == '1')
        {
            in >> ull{ input.key1 };
            keyOne = true;
        }
        else if (numOfKey == '2')
        {
            in >> ull8{ input.key2 };
            if (isOctal(input.key2)) {
                keyTwo = true;
            }
        }
        else if (numOfKey == '3')
        {
            in >> str{ input.key3 };
            keyThree = true;
        }
        in >> separate{ ':' };
    }
    in >> separate{ ')' };
    if (in && keyOne && keyTwo && keyThree)
    {
        dest = input;
    }
    else
    {
        in.setstate(std::ios::failbit);
    }
    return in;
}

std::ostream& operator<<(std::ostream& out, const DataStruct& src)
{
    std::ostream::sentry sentry(out);
    if (!sentry)
    {
        return out;
    }
    iofmtguard fmtguard(out);
    out << "(:";
    out << "key1 " << std::fixed << std::setprecision(1) << src.key1 << "ull:";
    out << "key2 0" << std::fixed << std::setprecision(1) << src.key2 << ":";
    out << "key3 \"" << src.key3 << '"';
    out << ":)";
    return out;
}

iofmtguard::iofmtguard(std::basic_ios< char >& s) :
    s_(s),
    fill_(s.fill()),
    precision_(s.precision()),
    fmt_(s.flags())
{}

iofmtguard::~iofmtguard()
{
    s_.fill(fill_);
    s_.precision(precision_);
    s_.flags(fmt_);
}

bool DataStructComparator::operator()(const DataStruct& left, const DataStruct& right)
{
    if (left.key1 < right.key1)
    {
        return true;
    }
    else if (left.key1 > right.key1)
    {
        return false;
    }
    else if (left.key2 < right.key2)
    {
        return true;
    }
    else if (left.key2 > right.key2)
    {
        return false;
    }
    else if (left.key3.size() < right.key3.size())
    {
        return true;
    }
    else
    {
        return false;
    }
}
