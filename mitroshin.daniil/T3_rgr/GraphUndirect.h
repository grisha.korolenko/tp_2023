#ifndef GRAPHUNDIRECT_H
#define GRAPHUNDIRECT_H
#include <iostream>
#include <iomanip>
#include <vector>
#include <stack>

class GraphUndirect {
public:
    GraphUndirect(const int&);
    GraphUndirect();
    GraphUndirect(const GraphUndirect& other);

    int getVertices();
    bool addEdge(const int&, const int&);
    bool isEdgeInTheGraph(const int&, const int&);
    bool removeEdge(const int&, const int&);
    void addVertex();
    bool isVertexInTheGraph(const int&);
    bool removeVertex(const int&);
    void DFS(const int&);
    void printGraph();
    bool isEmpty();

    GraphUndirect& operator=(GraphUndirect&& other) noexcept;

private:
    int numberOfVertices;
    std::vector<std::vector<bool>> matrix;
    void DFSUtil(const int&, std::vector<bool>&);
};

#endif

