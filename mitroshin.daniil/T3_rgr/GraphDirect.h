#ifndef GRAPHDIRECT_H
#define GRAPHDIRECT_H
#include <iostream>
#include <iomanip>
#include <vector>
#include <stack>

class GraphDirect {
public:
    GraphDirect(const int&);
    GraphDirect();
    GraphDirect(const GraphDirect& other);

    bool addArc(const int&, const int&);
    bool isArcInTheGraph(const int&, const int&);
    int getVertices();
    bool isVertexInTheGraph(const int&);
    bool removeArc(const int&, const int&);
    void addVertex();
    bool removeVertex(const int&);
    bool topologicalSort();
    void DFS(const int&);
    void printGraph();
    bool isEmpty();
    bool hasCycle();

    GraphDirect& operator=(GraphDirect&& other) noexcept;

private:
    int numberOfVertices;
    std::vector<std::vector<bool>> matrix;
    void topologicalSortUtil(const int&, std::vector<bool>&, std::stack<int>&);
    void DFSUtil(const int&, std::vector<bool>&);
    bool loopFinder(const int&, std::vector<bool>&, std::vector<bool>&);
};

#endif


