#include "DataStruct.h"

std::array<bool, 3> myArray = { 0, 0, 0 };
char global = '\0';

namespace nspace
{
    std::istream& operator>>(std::istream& in, DelimiterIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char c = '\0';
        in >> c;
        if (in && (c != dest.exp))
        {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, UnsignedLongLongIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        } 
        in >> DelimiterIO{ '0' } >> std::oct >> dest.ref;
        return in;
    }

    std::istream& operator>>(std::istream& in, DoubleScienceIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        std::string number = "";
        getline(in, number, ':');
        if (number.find('e') == std::string::npos && number.find('E') == std::string::npos)
        {
            in.setstate(std::ios::failbit);
        }
        else
        {
            in.unget();
            while (in.peek() != ' ')
            {
                in.unget();
            }
            in >> dest.ref;
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, StringIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        return std::getline(in >> DelimiterIO{ '\"' }, dest.ref, '\"');
    }

    std::istream& operator>>(std::istream& in, LabelStringIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        return std::getline(in >> DelimiterIO{ ':' }, dest.exp, ' ');
    }
    std::istream& operator>>(std::istream& in, LabelIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        std::string data = "";
        if (in >> LabelStringIO{ data })
        {
            char c = data[data.length() - 1];
            data.erase(data.length() - 1, 1);
            if (data != dest.exp || c < '1' || c > '3' || myArray[c - '0' - 1] == 1)
            {
                in.setstate(std::ios::failbit);
            }
            else
            {
                myArray[c - '0' - 1] = 1;
                global = c;
            }
        }
        return in;
    }
    std::istream& operator>>(std::istream& in, DataStruct& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        DataStruct input;
        {
            using sep = DelimiterIO;
            using label = LabelIO;
            using ull = UnsignedLongLongIO;
            using dbl = DoubleScienceIO;
            using str = StringIO;
            for (int i = 0; i < 3; i++)
            {
                myArray[i] = 0;
            }
            in >> std::skipws >> sep{ '(' } >> std::noskipws;
            for (int i = 0; i < 3; i++)
            {
                char c = in.peek();
                if (c != ':')
                {
                    in.setstate(std::ios::failbit);
                }
                in >> label{ "key"};
                if (global == '1')
                {
                    in >> std::skipws >> dbl{ input.key1 };
                }
                else if (global == '2')
                {
                    in >> std::skipws >> ull{ input.key2 };
                }
                else if (global == '3')
                {
                    in >> std::skipws >> str{ input.key3 };
                }
            }
            in >> std::noskipws >> sep{ ':' };
            in >> std::noskipws >> sep{ ')' };
        }
        if (in)
        {
            dest = input;
        }
        return in;
    }

    std::ostream& operator<<(std::ostream& out, const DataStruct& src)
    {
        std::ostream::sentry sentry(out);
        if (!sentry)
        {
            return out;
        }
        iofmtguard fmtguard(out);
        out << "(";
        out << ":key1 " << std::scientific << std::setprecision(3) << src.key1;
        out << ":key2 0" << std::oct << std::setprecision(3) << src.key2;
        out << ":key3 " << '\"' << src.key3 << '\"';
        out << ":)";
        return out;
    }

    bool sort(DataStruct a, DataStruct b)
    {
        bool first = (a.key1 < b.key1);
        bool second = (a.key1 == b.key1 && a.key2 < b.key2);
        bool third = (a.key1 == b.key1 && a.key2 == b.key2 && a.key3.size() < b.key3.size());
        return first || second || third;
    }

    iofmtguard::iofmtguard(std::basic_ios< char >& s) :
        s_(s),
        fill_(s.fill()),
        precision_(s.precision()),
        fmt_(s.flags())
    {}

    iofmtguard::~iofmtguard()
    {
        s_.fill(fill_);
        s_.precision(precision_);
        s_.flags(fmt_);
    }
}