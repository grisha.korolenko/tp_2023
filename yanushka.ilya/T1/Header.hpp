#ifndef DATA_STRUCT_H
#define DATA_STRUCT_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <sstream>

struct DataStruct {
    long long key1;
    unsigned long long key2;
    std::string key3;
};

bool isValidLIT(const std::string& value) {
    for (const char& temp : value) {
        if (!isdigit(temp)) {
            return false;
        }
    }
    return true;
}

bool isValidOCT(const std::string& value) {
    for (const char& temp : value) {
        if (!isxdigit(temp)) {
            return false;
        }
    }
    if (value.length() < 2)
        return false;

    if (value[0] != '0')
        return false;

    for (size_t i = 1; i < value.length(); ++i) {
        if (value[i] < '0' || value[i] > '7')
            return false;
    }

    return true;
}

std::istream& operator>>(std::istream& is, DataStruct& data) {
    std::string str;

    while (std::getline(is, str)) {
        if (str.front() != '(' or str.back() != ')') {
            continue;
        }

        std::stringstream ss(str.substr(1, str.size() - 2));
        std::string token;
        int validFields = 0;
        DataStruct temp;

        while (std::getline(ss, token, ':')) {
            std::stringstream inner(token);
            std::string key, value;

            inner >> key >> value;

            if (key == "key1") {
                std::string check = value.substr(value.size() - 2, value.size());
                std::string numStr = value.substr(0, value.size() - 2);
                if (isValidLIT(numStr) and (check == "ll" or check == "LL")) {
                    try {
                        temp.key1 = std::stoull(numStr);
                        validFields++;
                    }
                    catch (const std::exception&) {
                    }
                }
            }
            else if (key == "key2") {
                if (isValidOCT(value)) {
                    try {

                        temp.key2 = std::stoull(value);
                        validFields++;
                    }
                    catch (const std::exception&) {
                    }
                }
            }
            else if (key == "key3") {
                temp.key3 = value.substr(1, value.size() - 2);
                validFields++;
            }
        }

        if (validFields == 3) {
            data = temp;
            return is;
        }
    }

    is.setstate(std::ios::failbit);
    return is;
}

std::ostream& operator<<(std::ostream& out, const DataStruct& data) {
    out << "(:key1 " << std::dec << data.key1 << "ll:key2 0" << data.key2 << ":key3 \"" << data.key3 << "\":)";
    return out;
}

bool operator<(const DataStruct& left, const DataStruct& right) {
    if (left.key1 != right.key1) {
        return left.key1 < right.key1;
    }
    else if (left.key2 != right.key2) {
        return left.key2 < right.key2;
    }
    else {
        return left.key3.size() < right.key3.size();
    }
}

#endif // DATA_STRUCT_H

