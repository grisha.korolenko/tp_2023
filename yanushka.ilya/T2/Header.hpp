#ifndef HEADER
#define HEADER
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cstdlib>
#include <set>
#include <algorithm>
#include <numeric>
#include <functional>
#include <iomanip>

struct Point
{
	int x, y;
};
struct Polygon
{
	std::vector< Point > points;
};
struct ComparePoints {
	bool operator()(const Point& p1, const Point& p2) {
		return p1.x == p2.x && p1.y == p2.y;
	}
};
bool operator==(const Polygon& lhs, const Polygon& rhs) {
	return lhs.points.size() == rhs.points.size() &&
		std::equal(lhs.points.begin(), lhs.points.end(), rhs.points.begin(),
			ComparePoints{});
}
bool isRightFormat(std::string str)
{
	std::string str1 = str.substr(0, str.find(' '));
	if (std::all_of(str1.begin(), str1.end(), ::isdigit))
	{
		std::string str2 = str.substr(str.find(' ') + 1);
		std::string str3;
		std::string str4;
		std::string x_str;
		std::string y_str;
		int counter = 0;
		while (!str2.empty())
		{
			str3 = str2.substr(0, str2.find(' '));
			if (str3.at(0) == '(' && str3.back() == ')')
			{
				str4 = str3.substr(1, str3.length() - 2);
				size_t semicolon_pos = str4.find(';');
				if (semicolon_pos == std::string::npos)
				{
					return false;
				}
				else
				{
					std::string x_str = str4.substr(0, semicolon_pos);
					std::string y_str = str4.substr(semicolon_pos + 1);
					if (!std::all_of(x_str.begin(), x_str.end(), ::isdigit) || !std::all_of(y_str.begin(), y_str.end(), ::isdigit))
					{
						return false;
					}
				}
			}
			else
			{
				return false;
			}
			auto pos = str2.find(' ');
			if (pos == std::string::npos)
			{
				str2.clear();
			}
			else
			{
				str2.erase(0, pos + 1);
			}
			counter++;
		}
		if (counter == std::stoi(str1))
		{
			return true;
		}
		return false;
	}
	else
	{
		return false;
	}
}

std::istream& operator>>(std::istream& is, Point& point)
{
	std::string str;
	is >> str;
	str = str.substr(1, str.length() - 2);
	size_t semicolon_pos = str.find(';');
	if (semicolon_pos != std::string::npos) {
		std::string x_str = str.substr(0, semicolon_pos);
		std::string y_str = str.substr(semicolon_pos + 1);
		point.x = std::stoi(x_str);
		point.y = std::stoi(y_str);
	}
	return is;
}

std::istream& operator>>(std::istream& is, Polygon& polygon)
{
	std::string str;
	std::streampos prev_pos = is.tellg();
	std::getline(is, str);
	if (str.empty())
	{
		std::getline(is, str);
	}
	is.seekg(prev_pos);

	if (!isRightFormat(str))
	{
		is.setstate(std::ios::failbit);
		return is;
	}
	int num_points;
	is >> num_points;
	while (num_points)
	{
		Point point;
		is >> point;
		polygon.points.push_back(point);
		num_points--;
	}
	return is;
}

double area(const Polygon& polygon)
{
	if (polygon.points.size() < 3)
	{
		return 0.0;
	}
	std::vector<Point>::const_iterator prev = polygon.points.end() - 1;
	std::vector<double> products(polygon.points.size());
	std::transform(polygon.points.begin(), polygon.points.end(), products.begin(),
		std::bind([](const Point& p1, const Point& p2) { return p1.x * p2.y - p2.x * p1.y; }, std::placeholders::_1, *prev));
	double sum = std::accumulate(products.begin(), products.end(), 0.0);
	return std::abs(sum) / 2.0;
}

double AREA(std::string str, const std::vector<Polygon>& polygons)
{
	std::vector<Polygon> Shapes;
	if (str == "EVEN")
	{
		std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(Shapes), [](const Polygon& polygon) {
			return polygon.points.size() % 2 == 0; });
	}
	if (str == "ODD")
	{
		std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(Shapes), [](const Polygon& polygon) {
			return polygon.points.size() % 2 != 0; });
	}
	double total_area = std::accumulate(Shapes.begin(), Shapes.end(), 0.0,
		[](double acc, const Polygon& shape) {
			return acc + area(shape);
		});
	return total_area;
}

double AREA(const std::vector<Polygon>& polygons)
{
	if (polygons.size() > 0)
	{
		double sum = std::accumulate(polygons.begin(), polygons.end(), 0.0, [](double acc, const Polygon& p)
			{return acc + area(p); });
		return sum / polygons.size();
	}
	return 0;
}

double AREA(std::size_t size, const std::vector<Polygon>& polygons)
{
	std::vector<Polygon> Shapes;
	std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(Shapes), [size](const Polygon& polygon)
		{return polygon.points.size() == size; });
	double total_area = std::accumulate(Shapes.begin(), Shapes.end(), 0.0,
		[](double acc, const Polygon& shape) {
			return acc + area(shape);
		});
	return total_area;
}

double MAX(std::string str, const std::vector<Polygon>& polygons)
{
	if (str == "AREA" && polygons.size() > 0)
	{
		std::vector<double> areas(polygons.size());
		std::transform(polygons.begin(), polygons.end(), areas.begin(), std::bind(area, std::placeholders::_1));
		auto max_area = *std::max_element(areas.begin(), areas.end());
		return max_area;
	}
	if (str == "VERTEXES" && polygons.size() > 0)
	{
		std::vector<int> sizes(polygons.size());
		std::transform(polygons.begin(), polygons.end(), std::back_inserter(sizes), [](const Polygon& polygon) {
			return polygon.points.size();
			});
		auto max_size = *std::max_element(sizes.begin(), sizes.end());
		return max_size;
	}
	return 0;
}

double MIN(std::string str, const std::vector<Polygon>& polygons)
{
	if (str == "AREA" && polygons.size() > 0)
	{
		std::vector<double> areas(polygons.size());
		std::transform(polygons.begin(), polygons.end(), areas.begin(), std::bind(area, std::placeholders::_1));
		auto min_area = *std::min_element(areas.begin(), areas.end());
		return min_area;
	}
	if (str == "VERTEXES" && polygons.size() > 0)
	{
		std::vector<int> sizes(polygons.size());
		std::transform(polygons.begin(), polygons.end(), std::back_inserter(sizes), [](const Polygon& polygon) {
			return polygon.points.size();
			});
		auto min_size = *std::min_element(sizes.begin(), sizes.end());
		return min_size;
	}
	return 0;
}

int COUNT(std::string str, const std::vector<Polygon>& polygons)
{
	std::vector<Polygon> sizes;
	if (str == "EVEN")
	{
		std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(sizes), [](const Polygon& polygon) {
			return polygon.points.size() % 2 == 0; });
	}
	if (str == "ODD")
	{
		std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(sizes), [](const Polygon& polygon) {
			return polygon.points.size() % 2 != 0; });
	}
	if (std::all_of(str.begin(), str.end(), ::isdigit))
	{
		int count = std::stoi(str);
		std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(sizes), [count](const Polygon& polygon) {
			return polygon.points.size() == static_cast<std::vector<Point>::size_type>(count); });
	}
	int size = sizes.size();
	return size;
}
int ECHO(const Polygon& polygon, std::vector<Polygon>& polygons)
{
	int count = 0;
	auto it = polygons.begin();
	while (it != polygons.end())
	{
		if (*it == polygon)
		{
			it = polygons.insert(it + 1, polygon);
			count++;
		}
		++it;
	}
	return count;
}

int RIGHTSHAPES(const std::vector<Polygon>& polygons)
{
	int count = 0;
	for (auto& polygon : polygons) {
		for (auto& point : polygon.points) {
			auto next = polygon.points[(polygon.points.end() - polygon.points.begin()) - 1];
			if (point.x == next.x || point.y == next.y) {
				count++;
				break;
			}
		}
	}
	return count;
}
std::string PRINTENTER(const std::vector<Polygon>& polygons)
{
	std::ostringstream oss;
	for (const auto& polygon : polygons)
	{
		oss << polygon.points.size();
		for (const auto& point : polygon.points)
		{
			oss << " (" << point.x << ";" << point.y << ")";
		}
		oss << "\n";
	}
	return oss.str();
}

#endif
