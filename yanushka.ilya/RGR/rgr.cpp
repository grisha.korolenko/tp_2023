#include "rgr.h"
Myifstream::Myifstream(const char* input) {
    fileStream.open(input);
}

Myifstream::~Myifstream() {
    if (fileStream.is_open())
        fileStream.close();
}

bool Myifstream::is_open() {
    return fileStream.is_open();
}

bool Myifstream::eof() {
    return fileStream.eof();
}

void Myifstream::close() {
    fileStream.close();
}



Myifstream::operator bool() const {
    return fileStream.is_open();
}
