#ifndef _DICTIONARY
#define _DICTIONARY
#include <algorithm>
#include <sstream>
#include <vector>
#include <fstream>
#include <map>
#include <list>
#include <iterator>
#include <iostream>
#include <iomanip>

template <typename KeyType, typename MappedType>
struct Pair
{
	const std::pair<const KeyType, MappedType>& pair;
	Pair(const std::pair<const KeyType, MappedType>& src) :
		pair(src)
	{}
};

struct Command
{
	int firstCommand;
	std::string secondCommand;
};

struct FirstCommandIO
{
	int& ref;
};

struct SecondCommandIO
{
	std::string& ref;
};

std::istream& operator>>(std::istream& in, FirstCommandIO&& dest);
std::istream& operator>>(std::istream& in, SecondCommandIO&& dest);
std::istream& operator>>(std::istream& in, Command& dest);
template <typename KeyType, typename MappedType>
std::ostream& operator<<(std::ostream& out, const Pair<KeyType, MappedType>& src);

class iofmtguard
{
public:
	iofmtguard(std::basic_ios< char >& s);
	~iofmtguard();
private:
	std::basic_ios< char >& s_;
	char fill_;
	std::streamsize precision_;
	std::basic_ios< char >::fmtflags fmt_;
};

template <typename KeyType, typename MappedType>
class Dictionary
{
public:
	Dictionary() = default;
	Dictionary(const Dictionary<KeyType, MappedType>& scr) = delete;
	Dictionary(Dictionary<KeyType, MappedType>&& src) = delete;
	Dictionary<KeyType, MappedType>& operator= (const Dictionary <KeyType, MappedType>& src) = delete;
	Dictionary<KeyType, MappedType>& operator= (Dictionary <KeyType, MappedType>&& src) = delete;
	virtual ~Dictionary() = default;
	void insertData(const KeyType& key, size_t string);
	bool searchData(const KeyType& key) const;
	void deleteData(const KeyType& key);
	void printMenu() const;
	void makeTable(std::ostream& out);
	int size() const;
	int maxSize() const;
	bool checkWord(std::string& key);
private:
	std::map<KeyType, MappedType> data_;
};

void openFile(std::ifstream& in, std::string inputTextFile);
void closeFile(std::ifstream& in);

template <typename KeyType, typename MappedType>
void Dictionary<KeyType, MappedType>::insertData(const KeyType& key, size_t string)
{
	if (data_[key].size() == 0 || data_[key].back() != string)
	{
		data_[key].push_back(string);
	}
}

template <typename KeyType, typename MappedType>
bool Dictionary<KeyType, MappedType>::searchData(const KeyType& key) const
{
	return data_.find(key) != data_.end();
}

template <typename KeyType, typename MappedType>
void Dictionary<KeyType, MappedType>::deleteData(const KeyType& key)
{
	if (data_.find(key) != data_.end())
	{
		data_.erase(key);
		std::cout << "����� " << key << " �������\n";
	}
	else
	{
		std::cout << "����� " << key << " �� �������\n";
	}
}

template <typename KeyType, typename MappedType>
void Dictionary<KeyType, MappedType>::printMenu() const
{
	std::cout << "����" << std::endl;
	std::cout << "1. �������� ����� ��� ������ �������" << std::endl;
	std::cout << "2. ����� ����� �� �������� �������. ��� ����� � ������� ���������� � ������� �����" << std::endl;
	std::cout << "3. ������� ����� �� �������� �������" << std::endl;
	std::cout << "4. ������� ������� ����" << std::endl;
	std::cout << "����� ������ �������� - ��� ���������� ������ ���������" << std::endl;
}

template <typename KeyType, typename MappedType>
void Dictionary<KeyType, MappedType>::makeTable(std::ostream& out)
{
	out << "\n//////////////////////////////////////////////Table//////////////////////////////////////////////\n";
	out << std::right << std::setw(51) << "����� " << "������\n";
	std::copy
	(
		std::begin(data_),
		std::end(data_),
		std::ostream_iterator<Pair<KeyType, MappedType>>(out, "\n")
	);
}

template <typename KeyType, typename MappedType>
int Dictionary<KeyType, MappedType>::size() const
{
	return data_.size();
}

template <typename KeyType, typename MappedType>
int Dictionary<KeyType, MappedType>::maxSize() const
{
	return data_.max_size();
}

template <typename KeyType, typename MappedType>
bool Dictionary<KeyType, MappedType>::checkWord(std::string& key)
{
	int end = key.length() - 1;
	while (end >= 0 && (key[0] < 'a' || key[0] > 'z') && (key[0] < 'A' || key[0] > 'Z'))
	{
		key.erase(0, 1);
		end -= 1;
	}
	while (end >= 0 && (key[end] < 'a' || key[end] > 'z') && (key[end] < 'A' || key[end] > 'Z') && key[end] != '\'')
	{
		key.erase(end, 1);
		end -= 1;
	}
	if (end < 0)
	{
		return false;
	}
	char first = 'a';
	char last = 'z';
	char largeFirst = 'A';
	char largeLast = 'Z';
	if (key[0] >= first && key[0] <= last)
	{
		key[0] -= (first - largeFirst);
	}
	bool isHyphen = false;
	bool isApostrophe = false;
	for (size_t i = 0; i < key.length(); i++)
	{
		if (!(key[i] >= first && key[i] <= last) && !(key[i] >= largeFirst && key[i] <= largeLast))
		{
			if ((key[i] != '-' && (key[i] != '\'' || isApostrophe)) || isHyphen)
			{
				return false;
			}
			else
			{
				isHyphen = true;
				if (key[i] == '\'')
				{
					isApostrophe = true;
				}
			}
		}
		else
		{
			isHyphen = false;
		}
	}
	return true;
}

void openFile(std::ifstream& in, std::string inputTextFile)
{
	std::cout << "������� ���� � �������� �����:\n";
	std::cout << inputTextFile << std::endl;
	in.open(inputTextFile);
}

void closeFile(std::ifstream& in)
{
	in.close();
}

std::istream& operator>>(std::istream& in, FirstCommandIO&& dest)
{
	std::istream::sentry sentry(in);
	if (!sentry)
	{
		return in;
	}
	in >> dest.ref;
	if (dest.ref < 1 || dest.ref > 4)
	{
		in.setstate(std::ios::failbit);
	}
	return in;
}

std::istream& operator>>(std::istream& in, SecondCommandIO&& dest)
{
	std::istream::sentry sentry(in);
	if (!sentry)
	{
		return in;
	}
	getline(in, dest.ref, '\n');
	return in;
}

std::istream& operator>>(std::istream& in, Command& dest)
{
	std::istream::sentry sentry(in);
	if (!sentry)
	{
		return in;
	}
	Command input;
	{
		using strFirst = FirstCommandIO;
		using strSecond = SecondCommandIO;
		in >> std::skipws >> strFirst{ input.firstCommand };
		if (in.peek() == '\n')
		{
			input.secondCommand = "";
		}
		else
		{
			in >> std::skipws >> strSecond{ input.secondCommand };
		}
	}
	if (in)
	{
		dest = input;
	}
	return in;
}

template <typename KeyType, typename MappedType>
std::ostream& operator<<(std::ostream& out, const Pair<KeyType, MappedType>& src)
{
	std::ostream::sentry sentry(out);
	if (!sentry)
	{
		return out;
	}
	iofmtguard fmtguard(out);
	out << std::setw(50) << src.pair.first << " ";
	std::copy
	(
		std::begin(src.pair.second),
		std::end(src.pair.second),
		std::ostream_iterator<size_t>(out, " ")
	);
	return out;
}

iofmtguard::iofmtguard(std::basic_ios< char >& s) :
	s_(s),
	fill_(s.fill()),
	precision_(s.precision()),
	fmt_(s.flags())
{}

iofmtguard::~iofmtguard()
{
	s_.fill(fill_);
	s_.precision(precision_);
	s_.flags(fmt_);
}
#endif